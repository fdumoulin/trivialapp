using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrivialApp;

namespace TrivialAppTests
{
	[TestClass]
	public class CalculatorTests
	{
		[TestMethod]
		public void Add()
		{
			double operandA = 1;
			double operandB = 1;
			double expected = 2;

			double result = Calculator.add(operandA, operandB);

			Assert.IsTrue(result == expected);

		}
	}
}
