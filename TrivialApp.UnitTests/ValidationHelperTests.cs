using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrivialApp;

namespace TrivialAppTests
{
	[TestClass]
	public class ValidationHelperTests
	{
		[TestMethod]
		public void TestIsValidEmail()
		{
			string[] goodEmails = {"email@example.com","first.last@example.ca","first-last@example.quebec","email@subdomain.example.com",
									"firstname+lastname@example.com","1234567890@example.com","email@example-one.com",
									"email@example.name","email@example.museum","email@example.co.jp","JoHn.Doe@sUb.ExAmPle.cA"};

			string[] badEmails = { "", " ", "@", "name@com", "name@@a.com", "email@example.com (Joe Smith)", "email@-example.com", "email@111.222.333.44444",
									"#@%^%#$@#$@#.com", "asd@asd@example.com", "..name@abc.example.com", ".email@example.com",
									"email.@example.com", "email..email@example.com" };

			foreach (string email in goodEmails)
			{
				Assert.IsTrue(ValidationHelper.IsValidEmail(email), email);
			}

			foreach (string email in badEmails)
			{
				Assert.IsFalse(ValidationHelper.IsValidEmail(email), email);//
			}

		}
	}
}
