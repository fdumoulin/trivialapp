﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace TrivialApp
{
	public class ValidationHelper
	{
		public const string EMAIL_REGEX1 = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@{1}(?:[a-zA-Z0-9][a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,20}$";
		
		public static bool IsValidEmail(string email)
		{

			return Regex.Match(email, EMAIL_REGEX1).Success;


		}

	}
}
