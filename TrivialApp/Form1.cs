﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrivialApp
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				this.textBox3.Text = Calculator.add(Double.Parse(textBox1.Text), Double.Parse(textBox2.Text)).ToString();
				this.label4.Text = "";
			} catch (Exception ex)
			{
				Console.Error.WriteLine(ex);
				this.label4.Text = ex.Message;
			} 
			
		}

		private void button2_Click(object sender, EventArgs e)
		{
			
			if (ValidationHelper.IsValidEmail(textBox4.Text))
			{
				textBox4.BackColor = Color.White;
				textBox4.ForeColor = Color.Green;
				label1.ForeColor = Color.Green;
				label1.Text = "Valid email";
			} else
			{
				textBox4.BackColor = Color.Red;
				textBox4.ForeColor = Color.White;
				label1.ForeColor = Color.Red;
				label1.Text = "Invalid email";
			}
			
			
		}

		private void textBox4_TextChanged(object sender, EventArgs e)
		{
			textBox4.BackColor = Color.White;
			textBox4.ForeColor = Color.Black;
			label1.ForeColor = Color.Black;
			label1.Text = "";
		}
	}
}
